package expr1

sealed trait Expr
final case class Lit(get: Int)                extends Expr
final case class Neg(value: Expr)             extends Expr
final case class Add(left: Expr, right: Expr) extends Expr

case class ShowExpr(expr: Expr) {
  // An interpreter of Expr that formats a string representation
  def show(e: Expr): String = e match {
    case Lit(g)    => g.toString()
    case Neg(v)    => s"(-${show(v)})"
    case Add(l, r) => s"(${show(l)} + ${show(r)})"
  }
  def run: String = show(expr)
}

case class EvalExpr(expr: Expr) {
  // An interpreter of Expr that evaluates the expression to an Int
  def eval(e: Expr): Int = e match {
    case Lit(g)    => g
    case Neg(v)    => -eval(v)
    case Add(l, r) => eval(l) + eval(r)
  }
  def run: Int = eval(expr)
}

object Expr1 extends App {
  val term = Add(Lit(30), Neg(Add(Lit(-4), Neg(Lit(8)))))

  println(ShowExpr(term).run) // (30 + (-(-4 + (-8))))
  println(EvalExpr(term).run) // 42
}
