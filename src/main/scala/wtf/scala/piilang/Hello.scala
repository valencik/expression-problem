package wtf.scala.piilang

sealed trait PiiData
final case class Value(get: String) extends PiiData
final case class Key(get: PiiData)  extends PiiData

sealed trait PiiOps
final case class Redact(data: PiiData)   extends PiiOps
final case class Truncate(data: PiiData) extends PiiOps

object Examples {
  val simple = Value("hello world")
  val nested = Key(Key(Value("I'm in here!")))
}

object Interpreters {

  def show(p: PiiData): String = p match {
    case Value(v) => v
    case Key(g)   => "[" + show(g) + "]"
  }

  def show(p: PiiOps): String = p match {
    case Redact(d)   => s"Redact[${show(d)}]"
    case Truncate(d) => s"Truncate[${show(d)}]"
  }

  def truncateData(d: PiiData): PiiData = d match {
    case Value(v) => Value(v.slice(0, 3) + "...")
    case Key(d)   => Key(truncateData(d))
  }
  def eval(p: PiiOps): String = p match {
    case Redact(_)   => "<REDACTED>"
    case Truncate(d) => show(truncateData(d))
  }
}

object Hello extends App {
  def prog(d: PiiOps): Unit = {
    println(s"PiiData: ${d}")
    println(s"PiiData show: ${Interpreters.show(d)}")
    println(s"PiiData eval: ${Interpreters.eval(d)}")
  }
  prog(Redact(Examples.simple))
  prog(Truncate(Examples.nested))
}
