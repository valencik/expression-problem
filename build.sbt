lazy val root = (project in file(".")).settings(
  inThisBuild(
    List(
      organization := "wtf.scala",
      scalaVersion := "2.13.1",
      version := "0.1.0-SNAPSHOT",
      coverageMinimum := 75
    )
  ),
  name := "piilang"
)
